# Collage d'affiches
## Idée générale
Le joueur peut coller des affiches sur la surface des objets ou des véhicules.

## Spécifications
* Le joueur peut coller des affiches de façon illimitée.
* Le joueur peut choisir la position du centre de l'affiche.
* Le joueur peut ajuster la taille de l'affiche.
* Le joueur peut sélectionner le contenu de l'affiche par rapport à une liste de modèles téléchargée depuis un site Internet.
* Le joueur peut pré-visualiser le collage de l'affiche (en transparence, en surbrillance, etc).
* Le joueur qui se connecte peut voir les affiches qui ont été collées avant son arrivée.
* L'affiche peut être collée sur une surface verticale ou quasi-verticale (10° d'inclinaison maximum par rapport à la 
verticale).
* L'affiche est systématiquement collée par rapport à la verticale avec un angle de rotation aléatoire (voir complément).
* L'affiche est visible par tous les autres joueurs une fois collée.
* Les affiches collées sont conservées à la fermeture du serveur.
* Les affiches sauvegardées sont restaurées au lancement du serveur.

## Détails techniques
### Affiche et orientations
![Repère de l'affiche](https://bitbucket.org/CaravaneTeam/demo-lua/raw/master/flyers/repere-affiche.png "Repère de l'affiche")

### Liste des affiches disponibles
La liste des affiches est fournie au format JSON. Le format d'une réponse est le suivant.
```json
{
  "flyers": [
    {
	  "height": "100",
	  "name" : "flyer_name",
	  "url" : "https://example.com/flyer.jpg",
	  "width": "100"
	},
	{
	  "height": "100",
	  "name" : "flyer_name",
	  "url" : "https://example.com/flyer.jpg",
	  "width": "100"
	}
	// ...
  ]
}
```

